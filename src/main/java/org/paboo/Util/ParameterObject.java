/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.util;

import com.d_project.qrcode.QRCode;
import com.google.zxing.qrcode.encoder.ByteMatrix;

/**
 * @author Leonard
 */
public class ParameterObject {

    public final static String DEFAULT_CONTECT = "pChart";
    public final static String DEFAULT_ENCODING = "ISO-8859-1";

    public static boolean hasText(String str) {
        if (!(str != null && str.length() > 0)) {
            return false;
        }
        return "".equals(str.trim());
    }

    public static boolean hasInteger(Integer i) {
        return i != null;
    }

    public static boolean hasBoolean(Boolean b) {
        return b != null;
    }

    public static String convertByteArrays(byte[][] bas) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bas.length; i++) {
            for (int j = 0; j < bas[0].length; j++) {
                sb.append(bas[i][j]);
            }
            sb.append("\r\n");
        }
        return sb.toString();
    }

    public static ByteMatrix convertToZxingByteMatrix(QRCode qrcode) {
        ByteMatrix output = new ByteMatrix(qrcode.getModuleCount(), qrcode.getModuleCount());
        for (int row = 0; row < qrcode.getModuleCount(); row++) {
            for (int col = 0; col < qrcode.getModuleCount(); col++) {
                if (qrcode.isDark(row, col)) {
                    output.set(col, row, 1);
                }
            }
        }
        return output;
    }
}
