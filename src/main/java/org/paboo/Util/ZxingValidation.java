/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

/**
 * @author Leonard
 */
public class ZxingValidation {
    public static boolean checkEncoding(String encoding) {
        return StandardCharsets.ISO_8859_1.name().equals(encoding) || StandardCharsets.UTF_8.name().equals(encoding)
                || Charset.forName("Shift_JIS").name().equals(encoding);
    }

    public static boolean checkEcLevel(String level) {
        return "l".equals(level.toLowerCase()) || "m".equals(level.toLowerCase()) || "q".equals(level.toLowerCase())
                || "h".equals(level.toLowerCase());
    }

    public static boolean isNum(String num) {
        return Pattern.compile("^[0-9]+$").matcher(num).matches();
    }

    public static boolean isHex(String hex) {
        return hex.startsWith("0x") && Pattern.compile("^0x[0-9A-fa-f]{6}$").matcher(hex).matches();
    }

    public static boolean checkSize(String size) {
        if (size.contains("x")) {
            String[] sizea = size.split("x");
            return isNum(sizea[0]) && isNum(sizea[1]);
        }
        return false;
    }

    public static boolean checkFormat(String format) {
        return "png".equals(format) || "jpg".equals(format) || "jpeg".equals(format)
                || "gif".equals(format) || "text".equals(format) || "svg".equals(format);
    }
}
