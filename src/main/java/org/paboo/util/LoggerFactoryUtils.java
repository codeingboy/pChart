/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Logger Factory Utils
 *
 * @author Leonard
 */
public class LoggerFactoryUtils {
    private static LoggerFactoryUtils ourInstance = new LoggerFactoryUtils();
    private Logger logger;

    private LoggerFactoryUtils() {
    }

    public static LoggerFactoryUtils getInstance() {
        return ourInstance;
    }

    public LoggerFactoryUtils load(Class<?> clazz) {
        logger = LogManager.getLogger(clazz);
        return this;
    }

    public void debug(String s) {
        if (logger.isDebugEnabled()) {
            logger.debug(s);
        }
    }

    public void debug(String s, Throwable t) {
        if (logger.isDebugEnabled()) {
            logger.debug(s, t);
        }
    }

    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    public void info(String s) {
        if (logger.isInfoEnabled()) {
            logger.info(s);
        }
    }

    public void info(String s, Throwable t) {
        if (logger.isInfoEnabled()) {
            logger.info(s, t);
        }
    }

    public void warn(String s) {
        if (logger.isWarnEnabled()) {
            logger.warn(s);
        }
    }

    public void warn(String s, Throwable t) {
        if (logger.isWarnEnabled()) {
            logger.warn(s, t);
        }
    }

    public void error(String s) {
        if (logger.isErrorEnabled()) {
            logger.error(s);
        }
    }

    public void error(String s, Throwable t) {
        if (logger.isErrorEnabled()) {
            logger.error(s, t);
        }
    }

    public void error(Throwable t) {
        if (logger.isErrorEnabled()) {
            logger.error(t);
        }
    }

    public void trace(String s) {
        if (logger.isTraceEnabled()) {
            logger.trace(s);
        }
    }

    public void trace(String s, Throwable t) {
        if (logger.isTraceEnabled()) {
            logger.trace(s, t);
        }
    }

}
