/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.filter;

import com.google.gson.Gson;
import org.paboo.util.LoggerFactoryUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Leonard
 */
@WebFilter
@Component
public class ErrFilter extends OncePerRequestFilter {

    private LoggerFactoryUtils logger = LoggerFactoryUtils.getInstance().load(ErrFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        ResponseWapper wapper = new ResponseWapper(response);
        chain.doFilter(request, wapper);

        if (response.getStatus() >= HttpStatus.BAD_REQUEST.value() && response.getContentType().equalsIgnoreCase(MediaType.TEXT_PLAIN_VALUE)) {
            ServletOutputStream out = response.getOutputStream();

            StatusEntity status = new StatusEntity();
            status.setErrcode(response.getStatus());
            status.setErrmsg(new String(wapper.getBytesOutputStream().toByteArray()));

            String respBody = new Gson().toJson(status);
            response.setIntHeader("Content-Length", respBody.length());
            out.write(respBody.getBytes());
            out.flush();
            wapper.getBytesOutputStream().writeTo(out);
            wapper.flushBuffer();
            out.close();
            wapper.close();
        }
    }
}
