/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.chart.pdf417;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.pdf417.PDF417Writer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author Leonard
 */
public class ZxingUtil {
    private final static Logger logger = LogManager.getLogger(ZxingUtil.class);

    public static BitMatrix encode(Pdf417Entity p) throws WriterException {
        Map<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
        hints.put(EncodeHintType.CHARACTER_SET, p.getEncoding());
        hints.put(EncodeHintType.ERROR_CORRECTION, p.getErrorCorrectionLevel());
        hints.put(EncodeHintType.MARGIN, p.getMargin());
        return new PDF417Writer().encode(p.getData(), BarcodeFormat.PDF_417, p.getWidth(), p.getHeight(), hints);
    }
}
