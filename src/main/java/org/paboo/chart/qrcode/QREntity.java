/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.chart.qrcode;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.Serializable;

/**
 * @author Leonard
 */
public class QREntity implements Serializable {

    private static final long serialVersionUID = -7770987988957452715L;
    private String data;
    private Integer margin;
    private String encoding;
    private ErrorCorrectionLevel ecLevel;
    private Integer width, height;
    private Integer backgroundColor, foregroundColor;
    private String format;
    private Boolean needBase64;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getMargin() {
        return margin;
    }

    public void setMargin(Integer margin) {
        this.margin = margin;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public ErrorCorrectionLevel getEcLevel() {
        return ecLevel;
    }

    public void setEcLevel(ErrorCorrectionLevel ecLevel) {
        this.ecLevel = ecLevel;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Integer backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Integer getForegroundColor() {
        return foregroundColor;
    }

    public void setForegroundColor(Integer foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Boolean isNeedBase64() {
        return needBase64;
    }

    public void setNeedBase64(Boolean needBase64) {
        this.needBase64 = needBase64;
    }

}
