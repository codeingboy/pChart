/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.chart.qrcode;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.zxing.WriterException;
import org.paboo.codec.Base64;
import org.paboo.util.LoggerFactoryUtils;
import org.paboo.util.ParameterObject;
import org.paboo.util.ZxingImage;
import org.paboo.util.ZxingValidation;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * @author Leonard
 */
@RestController
@ExposesResourceFor(QREntity.class)
public class QrController {

    private static final long serialVersionUID = -3904063115792721985L;
    private LoggerFactoryUtils log = LoggerFactoryUtils.getInstance().load(QrController.class);

    @RequestMapping("/qr")
    public ResponseEntity<byte[]> handleControllerExecution(HttpMethod method,
                                                            @RequestParam(value = "data", required = false, defaultValue = ParameterObject.DEFAULT_CONTECT) String ctx,
                                                            @RequestParam(value = "encoding", required = false, defaultValue = ParameterObject.DEFAULT_ENCODING) String encoding,
                                                            @RequestParam(value = "eclevel", required = false, defaultValue = "l") String ecLevel,
                                                            @RequestParam(value = "size", required = false, defaultValue = "200x200") String size,
                                                            @RequestParam(value = "margin", required = false, defaultValue = "1") int margin,
                                                            @RequestParam(value = "bgcolor", required = false, defaultValue = "DEFAULT") String bgcolor,
                                                            @RequestParam(value = "fgcolor", required = false, defaultValue = "DEFAULT") String fgcolor,
                                                            @RequestParam(value = "format", required = false, defaultValue = "png") String format,
                                                            @RequestBody(required = false) String requestStr) throws IOException {

        QREntity p = new QREntity();
        MediaType mt = MediaType.TEXT_PLAIN;

        if (!ParameterObject.hasText(requestStr)) {
            requestStr = "{}";
        }

        if (method == HttpMethod.GET) {

            if (!(ZxingValidation.checkEncoding(encoding) && ZxingValidation.checkEcLevel(ecLevel)
                    && ZxingValidation.checkSize(size)
                    && (format.contains("|") ? ZxingValidation.checkFormat(format.split("\\|")[0].toLowerCase()) : ZxingValidation.checkFormat(format.toLowerCase()))
            )) {
                String errorMsg = "Data parameter ERROR!";
                log.warn(errorMsg);

                ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .contentType(mt)
                        .body(errorMsg.getBytes());
            }

            p.setData(URLDecoder.decode(ctx, encoding));
            p.setEncoding(encoding);
            p.setEcLevel(ZxingUtil.parseErrorCorrectLevel(ecLevel));
            p.setMargin(margin);

            String[] sizea = size.split("x");
            p.setWidth(Integer.parseInt(sizea[0]));
            p.setHeight(Integer.parseInt(sizea[1]));

            p.setBackgroundColor(ZxingValidation.isHex(bgcolor) ? Integer.valueOf(bgcolor.substring(2), 16) : ZxingImage.WHITE);
            p.setForegroundColor(ZxingValidation.isHex(fgcolor) ? Integer.valueOf(fgcolor.substring(2), 16) : ZxingImage.BLACK);

            p.setNeedBase64(false);
            if (format.contains("|")) {
                String[] f = format.split("\\|");
                format = f[0];
                p.setNeedBase64(f[1].equalsIgnoreCase("base64"));
            }
            p.setFormat(format.toLowerCase());
            log.info(new Gson().toJson(p));
        } else if (method == HttpMethod.POST) {
            //BufferedReader br = new BufferedReader(new InputStreamReader(requestIS, encoding));
            Gson gson = new Gson();
            try {
                p = gson.fromJson(requestStr, QREntity.class);
            } catch (JsonIOException | JsonSyntaxException ex) {
                String errorMsg = "Data parameter ERROR!";
                log.error(errorMsg);

                ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .contentType(mt)
                        .body(errorMsg.getBytes());
            }
            if (p == null) {
                p = new QREntity();
            }
            if (!ParameterObject.hasText(p.getData())) {
                p.setData(ParameterObject.DEFAULT_CONTECT);
            }
            if (!ParameterObject.hasText(p.getEncoding()) || !ZxingValidation.checkEncoding(p.getEncoding().toLowerCase())) {
                p.setEncoding(StandardCharsets.ISO_8859_1.name());
            }
            if (p.getEcLevel() == null) {
                p.setEcLevel(ZxingUtil.parseErrorCorrectLevel("l"));
            }
            if (!ParameterObject.hasInteger(p.getMargin())) {
                p.setMargin(1);
            }
            if (!ParameterObject.hasInteger(p.getWidth()) || !ParameterObject.hasInteger(p.getHeight())) {
                p.setWidth(200);
                p.setHeight(200);
            }
//            if (p.getWidth() != p.getHeight()) {
//                p.setWidth(Math.max(p.getWidth(), p.getHeight()));
//                p.setHeight(Math.max(p.getWidth(), p.getHeight()));
//            }
            if (!ParameterObject.hasInteger(p.getBackgroundColor())) {
                p.setBackgroundColor(ZxingImage.WHITE);
            }
            if (!ParameterObject.hasInteger(p.getForegroundColor())) {
                p.setForegroundColor(ZxingImage.BLACK);
            }
            if (!ParameterObject.hasText(p.getFormat()) || !ZxingValidation.checkFormat(p.getFormat().toLowerCase())) {
                p.setFormat("png");
            }
            if (!ParameterObject.hasBoolean(p.isNeedBase64())) {
                p.setNeedBase64(false);
            }
            log.info(gson.toJson(p));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                    .contentType(mt)
                    .body("Request error!!!".getBytes());
        }

//        if(!ZxingUtil.checkData(p)) {
//            String errorMsg = "Date content OR parameter ERROR!";
//            log.error(errorMsg);
//            response.setContentType("text/plain");
//            response.getOutputStream().print(errorMsg);
//            return;
//        }

        if ("text".equalsIgnoreCase(p.getFormat())) {
            String outMsg;
            try {
                outMsg = ParameterObject.convertByteArrays(ZxingUtil.encode(p).getArray());
            } catch (IllegalArgumentException | WriterException ex) {
                outMsg = "Create Fail!!!";
                if (log.isDebugEnabled()) {
                    log.error(outMsg, ex);
                } else {
                    log.error(outMsg);
                }
            }

            return ResponseEntity.ok()
                    .contentType(mt)
                    .body(outMsg.getBytes());
        }
        if ("svg".equalsIgnoreCase(p.getFormat())) {
            try {
                mt = new MediaType("image/svg+xml");
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                ZxingImage.toSVGDocument(ZxingUtil.encode(p),
                        new OutputStreamWriter(new BufferedOutputStream(out), StandardCharsets.ISO_8859_1.name()),
                        p.getWidth(), p.getHeight(), p.getMargin(), p.getForegroundColor(), p.getBackgroundColor());

                ResponseEntity.ok().contentType(mt)
                        .contentLength(out.size())
                        .body(out);
            } catch (Exception e) {
                String errorMsg = "Create Fail!!!";
                if (log.isDebugEnabled()) {
                    log.error(errorMsg, e);
                } else {
                    log.error(errorMsg);
                }
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(mt)
                        .body(errorMsg.getBytes());
            }
        }
        BufferedImage data;
        try {
            data = ZxingUtil.encodeWithImg(p);
        } catch (IllegalArgumentException | WriterException ex) {
            String errorMsg = "Create Fail!!!";
            if (log.isDebugEnabled()) {
                log.error(errorMsg, ex);
            } else {
                log.error(errorMsg);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .contentType(mt)
                    .body(errorMsg.getBytes());
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        if (!ImageIO.write(data, p.getFormat().toLowerCase(), out)) {
            String errorMsg = "Could not write an image of format ";
            log.error(errorMsg);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .contentType(mt)
                    .body(errorMsg.getBytes());
        }
        if (p.isNeedBase64()) {
            String base64 = Base64.encodeToString(out.toByteArray());

            return ResponseEntity.ok().contentType(mt)
                    .body(base64.getBytes());
        } else {
            if (p.getFormat().equalsIgnoreCase("png")) {
                mt = MediaType.IMAGE_PNG;
            } else if (p.getFormat().equalsIgnoreCase("gif")) {
                mt = MediaType.IMAGE_GIF;
            } else if (p.getFormat().equalsIgnoreCase("jpg") || p.getFormat().equalsIgnoreCase("jpeg")) {
                mt = MediaType.IMAGE_JPEG;
            } else {
                return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                        .contentType(mt)
                        .body("Not implement image".getBytes());
            }

            return ResponseEntity.ok().contentType(mt)
                    .contentLength(out.size())
                    .body(out.toByteArray());
        }


    }
}
