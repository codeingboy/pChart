/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.chart.qrcode;

import com.d_project.qrcode.QRCode;
import org.paboo.util.LoggerFactoryUtils;
import org.paboo.util.ParameterObject;
import org.paboo.util.ZxingImage;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * @author Leonard
 */
@RestController
@ExposesResourceFor(QrdEntity.class)
public class QrdController {

    private static final long serialVersionUID = 6490130504990685386L;
    private LoggerFactoryUtils logger = LoggerFactoryUtils.getInstance().load(QrdController.class);


    @RequestMapping("/qrd")
    public ResponseEntity<byte[]> handleControllerExecution(HttpMethod method,
                                                            @RequestParam(value = "data", required = false, defaultValue = ParameterObject.DEFAULT_CONTECT) String ctx,
                                                            @RequestParam(value = "type", required = false, defaultValue = "2") int type,
                                                            @RequestParam(value = "size", required = false, defaultValue = "1") int size,
                                                            @RequestParam(value = "margin", required = false, defaultValue = "1") int margin,
                                                            @RequestParam(value = "level", required = false, defaultValue = "L") String level,
                                                            @RequestParam(value = "format", required = false, defaultValue = "png") String format) throws IOException {

        MediaType mt = MediaType.TEXT_PLAIN;
        QrdEntity e = new QrdEntity();

        if (method == HttpMethod.GET || method == HttpMethod.POST) {

            if ((type < 0 || 10 < type)
                    || (margin < 0 || 32 < margin)
                    || (size < 0 || 32 < size)) {
                String errorMsg = "illegal number";

                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .contentType(mt)
                        .body(errorMsg.getBytes());
            }

            String encoding = StandardCharsets.ISO_8859_1.name();
            e.setData(URLDecoder.decode(ctx, encoding));
            e.setLevel(QRdUtil.parseErrorCorrectLevel(level));
            e.setType(type);
            e.setSize(size);
            e.setMargin(margin);
            e.setFormat(format);
        } else {
            String errorMsg = "Request error!!!";
            logger.error(errorMsg);
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                    .contentType(mt)
                    .body("Request error!!!".getBytes());
        }

        if ("text".equalsIgnoreCase(format)) {

            String outMsg;
            try {
                outMsg = ParameterObject.convertByteArrays(QRdUtil.getQRCodeBytes(e));
            } catch (IllegalArgumentException ex) {
                outMsg = "Create Fail!!!";
                if (logger.isDebugEnabled()) {
                    logger.error(outMsg, ex);
                } else {
                    logger.error(outMsg);
                }
            }

            return ResponseEntity.ok()
                    .contentType(mt)
                    .body(outMsg.getBytes());
        } else if ("svg".equalsIgnoreCase(format)) {
            mt = new MediaType("image/svg+xml");
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            QRCode qrcode = QRdUtil.getQRCode(e);
            int imageSize = qrcode.getModuleCount() * e.getSize() + e.getMargin() * 2;
            ZxingImage.toSVGDocument(
                    ParameterObject.convertToZxingByteMatrix(qrcode),
                    new OutputStreamWriter(new BufferedOutputStream(out), StandardCharsets.ISO_8859_1.name()),
                    imageSize, imageSize, e.getMargin(), ZxingImage.BLACK, ZxingImage.WHITE);


            return ResponseEntity.ok().contentType(mt)
                    .contentLength(out.size())
                    .body(out.toByteArray());
        }


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BufferedImage image = QRdUtil.getQRCodeImage(e);
        if (image == null || !ImageIO.write(image, e.getFormat().toLowerCase(), out)) {
            String errorMsg = "Could not write an image of format ";
            logger.error(errorMsg);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .contentType(mt)
                    .body(errorMsg.getBytes());
        }

        if (e.getFormat().equalsIgnoreCase("png")) {
            mt = MediaType.IMAGE_PNG;
        } else if (e.getFormat().equalsIgnoreCase("gif")) {
            mt = MediaType.IMAGE_GIF;
        } else if (e.getFormat().equalsIgnoreCase("jpg") || e.getFormat().equalsIgnoreCase("jpeg")) {
            mt = MediaType.IMAGE_JPEG;
        } else {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                    .contentType(mt)
                    .body("Not implement image".getBytes());
        }

        return ResponseEntity.ok().contentType(mt)
                .contentLength(out.size())
                .body(out.toByteArray());

    }
}
